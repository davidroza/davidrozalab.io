---
title: A Journey Across Disciplines
subtitle: From Chemistry to Data Science
comments: false
---

Starting my journey in the field of Chemistry, my career was dedicated to therapeutic advancements in cancer therapy and innovative materials in tissue engineering. In drug development, I worked in molecular analyses for cancer therapies, while in material science, I contributed to pioneering scaffolds for tissue engineering. This foundation catalyzed my transition into applied mathematics, where I discovered a profound affinity for mathematical modeling and programming.

As a data scientist and engineer, I now leverage this multidisciplinary expertise to drive innovation and inform strategic decision-making. My proficiency encompasses a comprehensive spectrum of data science tools and frameworks, ensuring adaptability in an ever-evolving technological landscape. My focus lies in extracting actionable insights from complex datasets, utilizing an advanced toolkit that includes machine learning, statistical modeling, and data visualization, and importantly deep understanding of the cloud and the infrastructure to process vast amount of data.

Crucially, my expertise transcends technical prowess, incorporating a strategic understanding of the social and business implications of data science. This holistic approach enables me to bridge the gap between raw data and impactful outcomes, and in driving data-driven transformations across diverse industries.
