---
title: Building a Personalized News Feed Telegram Bot
subtitle: Part 3 - Refactoring and Enhancing the Telegram Bot
date: 2023-12-22
tags: ["c++", "boost-asio", "openssl", "curl"]
---

After successfully piloting the initial version of the Telegram bot project and implementing necessary network security measures, it was time to take the code to the next level. This post details the significant improvements made to the project, focusing on cleaner code, more robust networking, and the integration of essential development practices.

<!--more-->

# Improved Networking

## Curl Library

The first approach leveraged the [Curl library](https://curl.se/docs/manpage.html), a versatile tool for transferring data using various protocols. Curl provided a relatively straightforward way to handle HTTP requests and responses, making it an excellent choice for interacting with RESTful APIs like Telegram's.

## Boost Asio and OpenSSL

The second, more advanced approach, utilized [Boost Asio](https://www.boost.org/doc/libs/1_83_0/doc/html/boost_asio.html) in conjunction with [OpenSSL](https://www.openssl.org/). This combination offered several advantages:

- **Low-level Control**: Boost Asio provides fine-grained control over network operations, allowing for more efficient and customizable implementations.
- **Asynchronous Operations**: Asio's asynchronous model enables better performance and scalability, although in this refactor it wasn't implemented since concurrency is not really required at this moment.
- **Secure Communications**: OpenSSL integration ensures that all communications with the Telegram API are encrypted.

### Implementation Details

The Boost Asio implementation required more setup but offered greater flexibility:

1. **Socket Creation**: Established a socket for network communication.
2. **SSL Stream**: An SSL stream was layered over the socket to handle encryption.
3. **Handshake Process**: Implemented the SSL handshake to establish a secure connection.
4. **Request Formatting**: Custom HTML headers were crafted for each request to the Telegram API.

## Integration of NewsData API

To enhance the bot's functionality, I integrated the [NewsData API](https://newsdata.io/), allowing searches for news worldwide. This integration uses Curl to fetch news articles based on user-specified criteria.

# News Command Implementation

Users can now use the `/news` command with the following syntax:

```
/news
q=<keyword, concerts, jobs, war, etc>
language=<en, fr, etc>
```

This command retrieves 10 articles matching the specified criteria and sends the URLs to the user.

# Bot Architecture Refactoring

The core `Bot` object underwent significant refactoring to improve modularity and extensibility:

- **Friend Class Design**: The Bot class was redesigned to act as a friend class to other components, allowing for more efficient access to Telegram API, NewsData API, and MongoDB elements.
- **Command Implementation**: I introduced a different approach to command handling using an unordered map of function pointes, allowing for dynamic command registration and easier maintenance.

#### Example of New Command Implementation

```cpp
telegram_bot.OnCommand(
 "start", [&telegram_bot](const TelegramBot::Message::pMessage &message) {
 telegram_bot.SendMessage(message->chat->chat_id, "Hi");
 });
```

This lambda-based approach provides a clean, state-aware method of defining bot commands, significantly improving code readability and maintainability.

## Testing, Logging, and Build Process Improvements

### Google Test Integration

[Google Test](https://github.com/google/googletest) was integrated into the project, enabling comprehensive unit and integration testing. This move towards test-driven development (TDD) has dramatically improved code quality and reliability.

### CMake Build System

Adopting [CMake](https://cmake.org/) as the build system brought several benefits:

- **Simplicity**: Easier management of project dependencies and build configurations.
- **Portability**: Enhanced cross-platform support, crucial for potential future expansions.
- **Integration**: Seamless integration with Google Test and other third-party libraries.

### Advanced Logging System

The logging system features six severity levels, each with its own color coding for easy visual distinction:

1. **TRACE** (Cyan): Logs minimal API functionality
2. **DEBUG** (Magenta): Logs functionality of newer components
3. **INFO** (Green): Logs normal functionality
4. **WARNING** (Yellow): Logs non-critical errors
5. **ERROR** (Red): Logs errors that may affect functionality
6. **CRITICAL** (Blue): Logs critical errors that severely impact the project

The logging level can be dynamically set using the `SetLevel(int)` method, allowing for flexible debug output control.

## Conclusion and Future Directions

This refactoring phase has significantly improved the Telegram bot's architecture, reliability, and maintainability. In upcoming posts, we'll explore the integration of NLP to query news of my own like.

[Previous post in the series](/post/2023-11-15-telegrambot)
