---
title: Analyzing car crashes in the US
subtitle: Part 1 - Understanding spatiotemporal variables
date: 2023-06-28
tags: ["python", "r", "web scrapping", "eda"]
---

A couple of months back I was getting smart at privacy. I was trying to understand the complexity of regulating data in the US. The current regulatory landscape is sectorial and for the most part is unapplicable to new sectors and types of data. That took me deep into a rabbit hole, and a very interisting one. As I was getting smart on this topic, I came across self-driving cars, which I never would have thought as a technology that could easily breach an individual's privacy. Regardless of privacy, I came across a claim from proponents of this technology, and which constitutes the entirety of the scope of this post: "94% of all car crashes are caused by human error".

<!--more-->

I could never find the basis of such claim, so I decided to scrape the data from the National Highway Traffic Safety Administration (NHTSA) and perform some analysis. You see, the problem is understanding what exactly is "human error", which can range from how we build our cars and infrastructure, to the way we drive.

The data is highly longitudinal and cross-sectional, it ranges from 1975 to 2020 and contains information about the accident, the people inside the car, the vehicle, and in later years there is even information about the weather, damage and many more points. A note on the data that is important to mention. Data management and administration standards have changed during the last 50 years, this means that multiple rows are only available for certain. In any case, let's see exactly how insightful the data are.

The code to the repository can be found [here](https://gitlab.com/davidroza/car-crashes).

First, I'd like to understand the behavior of car crashes from a longitudinal perspective. Since the human population, the number of cars and road infrastructure has progressively increased, I would expect the number of car crashes per year to display a similar trend, at least according to the overall premise.

Examining the yearly trend we observe that the total number of car crashes has progressively decreased during the last five decades.

#### Figure 1: **Car accidents have gradually decreased over the last five decades**

![yearly](/img/carcrashes/yearly_count.png)

It also displays an interesting behavior, where the number of accidents increase for several years, just to later decrease for another period, with the most pronounced inflection point ocurring in 2007. This plot also contains the local maxima to flag peaks of maximum intensity.

Upon more investigation, and by adding monthly information, we observe that car accidents have seasonality! With the lowest count being during the winter and the highest during summer.

#### Figure 2: **Car accidents display seasonality, high on summer and low on winter**

![monthly](/img/carcrashes/monthly_count.png)

The plot contains a blue line, a smoothed version of the data to better understand the patterns that emerge. The smoothing removes noisy information and returns a clear pattern with three valleys and three hills. Further preprocessing with moving averages over periods of 5 years, display that car crashes, in fact, are in their lowest period in the last 45 years. Another interesting fact is that car crashes seem to display stationarity in periods of 15 years. What could be driving such discontinuities?


#### Figure 3: **Accidents converge to three periods of stability. Two inflection points imply changes in driver behavior**

![mamonthly](/img/carcrashes/ma_month_count.png)

This result is counterintuitive, more cars and more people should increase the probability of car crashes, at least from a pure probabilistic statement. The fact that we observe the opposite trend and three different baselines across the temporal frame, imply that there have been succesful interventions or behavioral changes.

Let's see what geographical information can add to the picture.

#### Figure 4: **California, Texas and Florida have the highest rate of accidents**

![state](/img/carcrashes/state_count.png)

From a pure spatial point of view, the map offers very few clues of, and if any, patterns in the data. The east coast display the most variability, but it is also the most populated region in the continental US. Yet, it is easy to spot that California, Texas and Florida are the states with the most accidents overall. This trend remains basically the same across the three 15 year periods we observed in the time series. Although this trend seems constant, a decrease in accidents is evident from the plots, just note the change in scale.

#### Figure 5: **The entire continental US displays a reduction of car accidents**

![state_year](/img/carcrashes/state_count_year.png)

By transforming years into ranges, we esentially create categorical variables that can be matched with the spatial dimension, in this case the state. When we run a Chi Square test for independence, which determines if two categorical variables are are statistically significant. The test compares the observed frequencies to the frequencies you would expect if the two variables are unrelated. When the variables are unrelated, the observed and expected frequencies will be similar. Since we obtained a p-value of 2.2e-16 from this test, **we reject the null hypothesis and conclude that space and time are associated**. 

<!-- Let's explore more in the [next](/post/carcrashes) post. -->
