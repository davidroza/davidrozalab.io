---
title: Building a Personalized News Feed Telegram Bot
subtitle: Part 2 - Cybersecurity
date: 2023-11-15
tags: ["c++", "pfsense", "network", "cybersecurity"]
---

A successful deployment isn't just about being bug-free; it's equally crucial to safeguard against cybersecurity incidents. According to [Forbes](https://www.forbes.com/sites/chuckbrooks/2022/01/21/cybersecurity-in-2022--a-fresh-look-at-some-very-alarming-stats/?sh=627137bc6b61), the world, and particularly the United States, has witnessed a dramatic surge in cyber attacks. Any computer connected to the internet 24/7 inherently poses a security risk, necessitating appropriate measures to fortify one's network.

<!--more-->

# The Rising Tide of Cyber Attacks

Recent statistics paint a concerning picture:

- In 2023, the average cost of a data breach reached $4.45 million globally (IBM Cost of a Data Breach Report 2023)
- Ransomware attacks increased by 13% in 2022, a rise as big as the last five years combined (Verizon 2023 Data Breach Investigations Report)
- Small businesses are increasingly targeted, with 43% of cyber attacks aimed at small businesses (Accenture)

These figures underscore the critical importance of implementing robust security measures, especially for continuous deployment setups like the one described in this project.

# Implementing Network Security Measures

To protect my network, I implemented a multi-layered approach focusing on three key areas: port forwarding, dynamic DNS, and VPN traffic routing. This setup was achieved by configuring pfSense (an open-source firewall/router software distribution) with a network switch supporting VLAN segmentation. Additionally, I employed an access point compatible with this configuration.

While I won't divulge the specific technical details for security reasons, I'll explain the value each component adds to the project's overall security:

### Port Forwarding

Port forwarding allows specific services on your home or business network to be accessible from the internet while maintaining firewall protection. It's a crucial tool for:

- Selectively exposing services (like web servers or game servers) to the internet
- Controlling incoming traffic to specific devices or applications
- Minimizing the attack surface by only opening necessary ports

### Dynamic DNS (DDNS)

Dynamic DNS services bridge the gap between the changing IP addresses typically assigned by Internet Service Providers (ISPs) and the need for a consistent address for hosted services. Benefits include:

- Maintaining accessibility to self-hosted services despite IP changes
- Enabling the use of domain names instead of IP addresses for easier management
- Facilitating remote access to home networks or services

### VPN (Virtual Private Network)

Routing all traffic through a VPN adds a powerful layer of security:

- Encrypts all inbound and outbound network traffic
- Masks the true IP address of your network, adding a layer of anonymity
- Protects against man-in-the-middle attacks and other forms of network-based threats

### Advanced Network Configuration

The implementation leveraged pfSense for its robust firewall capabilities and VLAN support. VLANs (Virtual Local Area Networks) allow for network segmentation, providing:

- Isolation of critical systems from potentially vulnerable devices
- Improved network performance through traffic segregation
- Enhanced security by containing potential breaches to specific segments

This setup extends protection to external hard drives connected to the network, ensuring that data storage remains secure even as it's actively used for fetching and processing news articles. With these security measures in place, the risk of a cyber attack against the Raspberry Pi and external hard drive setup is significantly reduced.

[Previous](/post/2023-03-02-telegrambot) and [next](/post/2023-12-22-telegrambot) post in the series.
