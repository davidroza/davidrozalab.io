---
title: Personalized news using Telegram
subtitle: Part 5 - Deploying into production
date: 2024-01-18
draft: true
tags: ["pfsense", "network"]
---

# Putting everything together and next steps

Since I was running my bot on a rasbperry pi, I had to first set it up. I used the 64-bit Raspberry Pi 4 Lite OS because and connected my desktop through ssh. The second step was intalling MongoDB and mongo-c-driver and mongocxx. Unfortunately MongoDB version 6 cannot be installed through apt, so I used a cross compiled version for the rpi that you can find [here](https://codeberg.org/samsamros/mongodb-raspberry-pi4), the set the appropriate group and user with permissions, create the mongo.conf file and set the mongod.service daemon. After all those steps, I installed mongocxx and all of its dependencies. Since I wanted my bot to run in the background as a service, I set up a daemon.

With all this text about specific events in the world, the next natural place to continue working is in creating a knowledge graph. That might take a bit longer because of all of the processing that is required, but the final product might be very interesting to analyze with ML and graph algorithms.

[Previous post](/post/2024-12-03-telegrambot).
