---
title: Personalized news using Telegram
subtitle: Part 4 - NLP classifier and crawler
date: 2024-01-01
draft: true
tags: ["python", "nlp", "ai", "transformers"]
---

The next stage was collecting and processing all the necessary data to train a news classifier. This required several weeks of reading the news, and adding them to the database with their corresponding score. The text from the article was mined, preprocessed to remove HTML tags, special characters and URLs, and standardized to lower case and only English characters. An NLP classifier was trained with the data and used as part of a crawler to fetch news of my liking.

<!--more-->

Using the bot's function /add, I collected news from a diverse set of topics and appended a score based on my preferences. The inclusion criteria considered the topic, the writing, visualizations, and relevance. News articles were only scrapped from journals and webpages that had no restrictions and policies against it, or those that did not have a _robots.txt_ page. You can find the repository [here](https://gitlab.com/davidroza/nlp-model).

# Preprocessing

The initial stage of the process required saving the url, date and score for a given article. A script periodically retrieves news articles that haven't been preprocessed in the database and run the following operations: retrieving the text from the article, removing HTML tags, links and stop words, and finally saving the standardized text in the database. During this preprocessing, the most relevant keywords are extracted for further functionality.

The database schema looks like this:

```
url : link to the original article
source : journal or webpage where the article was retrieved
date : date in UTC format
score : a score between 1 - 10
text : original text
content : processed text
keywords : list of most relevant keywords
processed : boolean to indicate whether text has been extracted and standardized
train : boolean to indicate whether the text has been used to train the classifier
```

Simple and effective. And the gret thing about MongoDB is its ability to easily add more key/value pairs if required at a later stage.

# NLP Classifier

On the backend, the article's text would be preprocessed and used to train a classifier. Initially the article's url, journal, and score would get stored in the database. At a later process the text would get extracted using the lxml module and then saved into the database. The data would then queried from the database and preprocessed into tokens using a lazy approach. Using batch updated, the data was used to fine-tune a BERT model for 10 classes (the scores from 1-10). For this section I used the pytorch and transformers modules for NLP.

# Crawler

With the aid of the classifier, the crawler would then have access to several news websites and periodically crawl the content to find the highest ranking content based on my specific preferences.

[Previous](/post/2023-11-15-telegrambot) post in the series.

<!-- and [next](/post/2024-01-01-telegrambot) post in the series. -->
