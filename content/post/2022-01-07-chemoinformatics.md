---
title: Exploring Parkinson disease
subtitle: Part 2 - Exploratory Data Analysis
date: 2022-01-07
tags: ["python", "r", "eda"]
draft: true
---

I've been working on this project for some time already. I had to bring my programming, data science and machine learning skills to the point where I would feel confident to pursue this project. After constructing the ETL with millions of molecules, I moved into analyzing the molecular space of drugs used for treating Parkinson disease.

<!--more-->

The analytical portion of this project is divided in several parts. First in exploratory data analysis to understand what kind of information the database contains, and how they impact the molecular space. Then I use classical machine learning techniques for activity prediction. And finally I move to deep learning techniques for molecule design. Code to the repository can be found [here](https://gitlab.com/davidroza/chemoinformatics).

This first post will cover exploratory data analysis and variable analysis. As any analytical technique should start, I began by preprocessing molecules into their canonical SMILES format. I used Rdkit to correct the charges and bonds, remove salts, and standardize aromaticity and stereochemistry. I then created a correlation matrix to understand the association between molecular characteristics, including aromaticity, Lipinski's rule of five, and presence of heavy atoms.

#### Figure 1: **High correlation between molecular variables can impede ML performance**

![corr](/img/chemoinformatics/correlation_matrix.png)

The correlation matrix displays a strong positive association between most variables and seem to be unfit for ML. The initial subset of variables only contain molecular information, and seems to be insufficient for predictive purposes. Yet, we can still salvage some variables for analytical purposes. We can more or less draw the same conclusion from the PCA plot, drug activity cannot be easily studied with these variables

#### Figure 2: **PCA confirms very low differentiation between active and inactive molecules**

![pca](/img/chemoinformatics/pca.png)

Let's dig deeper.

[Previous post in the series](/post/2021-12-30-chemoinformatics)
