---
title: Computer Vision on Beach Cleanup Data
subtitle: A Surfrider Foundation Pilot Project
date: 2024-08-24
tags:
  [
    "data science",
    "data engineering",
    "gcp",
    "computer vision",
    "machine learning",
  ]
---

Recently, I embarked on an exciting volunteer project with the Surfrider Foundation, a non-profit organization dedicated to the protection of the world's oceans. The project aims to develop an automation tool to process volunteer beach cleaning cards with computer vision - streamlining a crucial data collection process for the organziation. This initiative is currently in its pilot phase, with the potential for a nationwide implementation if successful.

<!--more-->

## Background and challenge

The Surfrider Foundation, established in 1984, is a grassroots environmental organization that works to protect and preserve the world's marine environments. With over 80 chapters across the United States, they organize regular beach cleanups with the help of thousands of volunteers. Every weekend, Surfrider chapters across the United States organize beach cleanups. Volunteers receive cards to record details of the trash collected during their efforts, which is used for 1) quantifying the types and amounts of debris found on beaches, 2) identifying trends in marine pollution, and 3) informing policymakers and the public about the state of our coastal environments. However, the current process requires manual reviewing and inputting of this data by Surfrider employees. With the growing number of volunteers and cleanups, this manual approach has become a significant bottleneck in Surfrider's data analysis and reporting capabilities.

## The Proposed Solution: Automation through Computer Vision

To address this challenge, the organization needed a data pipeline capable of processing volunteer cards to a suitable analytical form. Implied in that pipeline is the digitization of the cards to PDF, the extraction of the categories and corresponding digits, and the classification of each digit. With those requirements in mind, the proposed architecture was designed to leverage computer vision and machine learning algorithms in a microservice architecture in Google Cloud Platform (GCP) where each service has its own role in processing the cards, and intermediate and final data are stored in storage buckets and a PostgreSQL database respectively:

1. **Processing**: This module handles the initial conversion of scanned PDF cards, extracting relevant tables and isolating fields containing volunteer handwriting.

2. **Segmentation**: Using the OpenCV library, each field is processed to detect individual handwritten digits.

3. **Classification**: A lightweight classifier built with the Scikit-learn library processes each segmented digit.

#### Figure 1: **Initial architecture**

![Surfrider Foundation Pilot Project Architecture](/img/surfrider/architecture.png)

However, there was a major problem. Even though the organization wanted a tool on the cloud, for processing the cards and the accessing the data, they also had price sensitivity during the pilot phase. This required pivoting from our original cloud architecture to a hybrid solution capable of working in the cloud and in desktop! The organization was very happy with this approach becuase it would allow the organization to use the tool, document areas of improvement and most importantly expedite their workflows, while still balancing the need for a future migration to the cloud.

#### Figure 2: **Revised architecture**

![Surfrider Foundation Pilot Project Architecture](/img/surfrider/architecture2.png)

With the organization's approval, we refactored our original proposal to work in their desktop machines, fortunately they all used the same OS, and worked with the same volume of data. We also added Terraform and GitHub actions to automate our deployments, infrastructure and images in sync.

## Data Science

As described above, the tool required the digitization of the physical cards using a scanner, the transformation of the PDF files into images, the extraction and classification of each individual digit. By breaking down the problem into these discrete steps, the problem is significantly simplified from a complex multi-digit handwriting recognition task to a MNIST challenge. The heavy lifting operations are not the classification of the digits, but rather the extraction of the digits, which is a simple operation using OpenCV. To illustrate the solution:

#### Figure 3: **Original and processed field**

![Original Field Before Processing](/img/surfrider/gray.png)

![Processed Field After Segmentation](/img/surfrider/final.png)

After segmenting the numbers, each individual digit was processed by a classification model. The best performing model was a Support Vector Machine (SVM) classifier on the MNIST dataset, achievied a 97% accuracy - a good start for the pilot project. Originally, our debuging tool was an in-house developed tool to observe each card, yet as the complexity of the project progressed the requirements for proper experiment tracking and debugging called for using MLflow.

#### Figure 4: **Debug tool with each detection field**

![Debug Tool](/img/surfrider/debug.png)

The debug tool shows how the original field has a bounding box around any detected digit, and how it is preprocessed to match the training set. If no digit is detected, then it defaults to zero.

## Project Implications and Future Prospects

The Surfrider Foundation is currently evaluating this pilot project to determine its potential for wider adoption. A nice logo always helps:

#### Figure 5: **Project Lighthouse Logo**

![Logo](/img/surfrider/lighthouse_logo.png)

Jokes aside, our success criteria include:

1. Simplifying at least 75% of the current manual workload
2. Accurately flagging cards that may contain potential errors for human review

In the coming months, we'll be closely monitoring the performance of this system and gathering feedback from Surfrider staff and volunteers. If successful, this project could be scaled nationwide, significantly enhancing the Surfrider Foundation's capabilities through data-driven advocacy and action.

Thank you for reading.
