---
title: Exploring Parkinson disease
subtitle: Part 1 - Extract, Transform, Load
date: 2021-12-30
tags: ["rdkit", "python", "etl"]
draft: true
---

One of my learning objectives during the COVID-19 pandemic was starting on Chemoinformatics. The first step was finding and curating quality chemical data; unfortunately, open access chemical data is a very scarse resource. After extensive research, I found there there are two very useful resources: [PUBCHEM](https://pubchem.ncbi.nlm.nih.gov/) and [ChemBL](https://www.ebi.ac.uk/chembl/).

<!--more-->

This series is intended as a high level walk-through of my experience writing an ETL from scratch. This post, is a small introduction to my first approaches and my unsuccesfull attempts; it also serves as a prelude to the series in Chemoinformatics.

### A First Approach with Python

The first step was downlaoding the data in SDF format with a webcrawler. Being totally naïve, I decided to first use Python, rdkit and pymongo to store the data in a database in MongoDB. The process was simple, unzip the file, load the data into a pandas dataframe (with the help of rdkit), bulk upload the molecules to the database and finally close the file. There were to many problems with this approach. To start, Python's memory management was so annoying that I had to restart the kernel and continue the process by accessing the logs.

Opening a file with 500k compounds is undoubtedly going to saturate your memory. I thought maybe a lazy approach would be memory efficient, but it still brilliantly failed due to Python's reference count. After some back-and-forth, I realized the ETL would not work at all if I had to manually restart the kernel. This was a big problem: I could not fully automate the transformation and loading steps. This completly defeated the purpose of a ETL process.

**Now what?**

To overcome this problem, I decided to use Python's [multiprocessing](https://docs.python.org/3/library/multiprocessing.html) module. Every time a file was extracted, opened and uploaded a fork was created where all the objects momentarily lived. This worked particularly well, any object created inside that process would be instantly deleted just before the beginning of the next fork, hence memory overflow was prevented. Still, the process was extremely slow. In fact, it was so slow that it took five continous days to upload all the information to the database. Additionally, rdkit preprocessed the data in ways that were not useful for the analysis I intended to conduct. To make things even worse, the code I wrote was not reusable, nor maintanable, nor anything useful outside of being a useful script.

### What went wrong?

Nobody is born knowing how to program an ETL, I had to learn the hard way. In fact, during the time being, I've learned best practices on how to handle bad data, errors, logging and data lineage. As Seneca says: “Luck is what happens when preparation meets opportunity.” With that in mind, I admit my biggest mistake was not planning accordingly, I now know for the next time I write an ETL.

The following posts in this series will detail the analysis of the ingested data.

[Next post in the series](/post/2023-09-01-chemoinformatics)
