---
title: Gun violence in America
subtitle: Predicting the number of victims
date: 2023-11-30
tags: ["r", "prediction", "mixture-models", "regression"]
---

Mass shootings have become so frequent that we do not know when can indiscriminate gunfire can happen. Gun fire has occured in theaters, schools, offices, and even in places of worship.

<!--more-->

The magazine Mother Jones has compiled, and made publicly accessible, a database of mass shootings in America. Data ranges from 1982 to 2023 and contains information on the total number of victims, the age of the shooter, latitude and longitude, type of gun and even signs of mental illness. The link to the dataset can be found [here](https://www.motherjones.com/politics/2012/12/mass-shootings-mother-jones-full-data/) - it can also be found in the [repository](https://gitlab.com/davidroza/shootings).

Mass shootings are bizarre, they only target civilians. This dataset shows that in **70%** of the incidents, perpetrators obtain their guns legally, but only **48%** are confirmed to have had prior signs of mental health issues. This is a very interesting insight, mental illness is not a strong predictor of an attack. It sounds counterintuitve but experts have talked about this in the [past](https://www.columbiapsychiatry.org/news/mass-shootings-and-mental-illness), implying the mental health componenet has blown a bit out of proportion. At the same time, mass shootings are a small portion of gun related violence in America, and the issue cannot be reduced to [easy access](https://www.washingtonpost.com/opinions/2021/03/19/what-connects-americas-mass-shootings-easy-access-guns/) to guns. Other environmental stressors (or the interplay of many) must be shapping the problem.

# Insights

Due to the nature of the process, there shouldn't be any form of spatial or temporal dependence in the data. All of theses cases are not connected to each other. Let's make a visual inspection and gather some insights about the data.

#### Figure 1: **Mass shootings occur all across America**

![map](/img/shootings/map.png)

The map displays mass shootings where coordinate data is available (approximately 90% of the data), the size of the circle encapsulates the total number of fatal and non-fatal victims. The two most important messages from this plot are 1) the need for removing outliers, such as the _2017 Las Vegas shooting_ with 600 total victims, and 2) the apperance of no underlying spatial distribution - concluding that it is a randomly generating process and there is no spatial dependence.

The data shows the number of accidents and the number of incidents are highly correlated. Shootings have not become deadlier, but rather they have become much more frequent. With the highest peak during the period between 2016 and 2018, just to decrease during the COVID-19 pandemic, most likely due to lockdown. It is also evident that mass shootings are less frequent during holiday seasons, such as January, May and August. However, during mid-summer shootings become much more lethal. Figure 2 and 3 provide some intuition on the some of the stressors. January, May and August are months when there is a change in pace and environment: January with Christmas and New Years holidays, and May and August with the beggining and end of the summer holidays. Yet, the spike in violence during the middle of the summer seems to indicate that perpetrators spend too much time isolated (e.g. friends and family away travelling, lack of a stronger rutine, etc.)

#### Figure 2: **Incidents have doubled in the last decade**

![year](/img/shootings/yearly_count.png)

Histogram shows incidents in <span style="color:#69b3a2">green</span> and the line the number of total victims in <span style="color:#CD5C5C">red</span>.

#### Figure 3: **Shootings are less frequent during the holidays but more lethal during mid-summer**

![month](/img/shootings/monthly_analysis.png)

Plot shows the number of incidents (left) and the number of total victims (right) as a function of the month.

By inspecting the relative change of total number of victims on a monthly basis, the three clusters of high volatility become apparent during the periods of 2008-2010, 2012-2014 and 2016-2020, where there are spikes in lethality and frequency. Before that, shootings were much less frequent. Now, interestingly, there were spikes during the 2008 financial crisis, but they weren't as volatile as the other two. What has been responsible for this change?

#### Figure 4: **Volatility analysis shows three clusters**

![volatility](/img/shootings/volatility.png)

<br>
<br>

# Populations

A deeper analysis of age data shows two underlying populations of perpetrators, which overlap around 30 years old. A visual inspection of the histogram of ages provides some evidence, and it can be confirmed by different methods. First by training a model to find populations based on Gaussian mixture models, since the Central Limit Theorem is expected to be applicable for ages. The green and red lines in Figure 5 backup the assumption of two populations overlapping around 30.

#### Figure 5: **Age distribution of perpetrators displays two overlapping populations**

![mm](/img/shootings/age_mm.png)

The second method consists of rethinking the problem as a state-based process. There are two populations and mass shootings shift between them, each population is a state in this model, and implyies a generative process that picks a random perpetrator from each population - this is refered as a change of state. This model yields the same results, a clear cut between perpetrators around 30 years old. From Figure 7 and 8, it is easy to derive that the red population is the most frequent shooter and the green population of the deadlier shootings.

#### Figure 7: **Shooters over 30 years old are the most frequent perpetrators**

![groups](/img/shootings/age_groups.png)

#### Figure 8: **Shooters under 30 years old are more lethal**

![age](/img/shootings/age_date.png)

<br>
<br>

# Regression

Is it possible to predict the number of victims just based on the a few variables? Since this is count data, there are some assumptions that are violated with linear regressions, this requires generalized linear models that consider other types of response variables. A Poisson regression is completely out of the question because the data displays high overdispersion (see Table 1). A much more appropriate model would be a negative binomial regression that doesn't need to satisfy the Poisson assumption of *mean == variance* for the response variable.

#### Table 1: **Mean and variance for each population**
| Group    | Mean | Variance |
|----------|------|----------|
| Under 30 | 19.2 | 367.9    |
| Over 30  | 11.8 | 81.8     |

The AIC values for the regression models controlled by certain variables are intriguing. All of the models user age as primary predictor of total victims, and then use other variables to expand the model, as seen in equation 1. From these results, **1)** race is not a predictor of total victims, **2)** place (religious, school, workplace, military, etc) is a stronger predictor for the population over 30, **3)** state and age is a worst predictor than only age, **4)** age, race, place, state and mental health is a much better predictor of total victims for the population under 30 years old, but much worse for the population over 30, and finally **5)** removing state does not improve the model when the other variables are present.

```
total victims ~ age + variable           (Eq. 1)
```
<br>

#### Table 2: **AIC for regression models based on age and categorical variables**
| Group    | Age | Age + Race | Age + Place | Age + State | All + MH | All + MH - State |
|----------|:---:|:----------:|:-----------:|:-----------:|:--------:|:----------------:|
| Under 30 | 437 |    434     |     441     |     446     | **418**  |       437        |
| Over 30  | 450 |    453     |     446     |     461     | **459**  |       449        |

Note: **All** includes race, place and state. MH is mental health.

The observed values are in <span style="color:red">red</span> and the predicted values are in <span style="color:gray">gray</span> (notice the change in scale between each population). Overall both models fail at bigger spikes, but provide a much better fit for the younger populations than for the older population.

#### Figure 9: **Observed and predicted total shooting victims, per age under 30**

![under](/img/shootings/under30.png)

#### Figure 10: **Observed and predicted total shooting victims, per age over 30**

![over](/img/shootings/over30.png)

If these results are correct, they imply that shootings perpetrated by **younger populations are much more driven by mental health**, while **older populations by the place where they are commited**. These results provide interesting clues to what motivates their behavior.