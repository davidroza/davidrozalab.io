---
title: "Reinforcement Learning: Adventures in SuperTuxKart"
subtitle: Navigating the Promises and Pitfalls of AI
date: 2021-12-22
tags: ["ai", "pytorch", "python"]
---

Over the past month, I've immersed myself in the fascinating world of Reinforcement Learning (RL), perhaps a little too deeply. The premise seems deceptively simple: define what's good and bad for an agent, then let it learn through trial and error. But as I quickly discovered, the reality is far more complex.

<!--more-->

In this post, I'll share my experience implementing the REINFORCE algorithm in SuperTuxKart using PyTorch and Ray. We'll explore the challenges faced, strategies employed, and insights gained.

![RL Meme](/img/reinforcement/reinforcementl1.png)
_The expectation vs. reality of Reinforcement Learning (Source: [scott.ai](https://scott.ai/))_

# The Challenge: SuperTuxKart Soccer

For those unfamiliar, SuperTuxKart is a 3D open-source racing game with various game modes. My task was to build an RL model capable of playing soccer in a 2v2 setting against the game's native AI. Why SuperTuxKart? Its open-source nature and diverse environments make it an excellent testbed for RL algorithms. The initial setup involved extracting key information:

- Positions of players
- Locations of goals
- Position of the ball

This data was transformed into local coordinates to feed into the model. The policy design focused on geometric properties of the feature space, with a strict reward system that only acknowledged positive actions.

# The REINFORCE Algorithm

REINFORCE is a policy gradient method in RL. It works by directly optimizing the policy without needing to estimate action-value functions. In essence, it adjusts the policy to make good actions more likely and bad actions less likely based on the rewards received.

### Reward Function Design

The reward function aimed to:

1. Minimize the distance between the player and the ball
2. Direct the ball towards the opposing goal

This approach significantly improved agent performance compared to updating based solely on score.

### Performance Metrics

The goal was to minimize the overall sum of distances throughout the game.

### Challenges Encountered

1. **Vanishing Gradients**: Often, the training would stall due to gradients becoming too small.
2. **Local Minima**: The agent would sometimes get stuck in suboptimal behaviors.
3. **Policy Design**: Sparse policies failed to learn, while continuous policies led to erratic behavior.

Addressing these issues required multiple iterations and careful tuning of the reward function and state representation.

# Results and Evaluation

After several training iterations, the model showed promising results:

#### Figure 1: **Agent is rewarded when distance to the ball is reduced**

![Agent Learning](/img/reinforcement/reinforcementl2.gif)

The agent evaluation focused on observing the decrease in the opponent's maximum and average score. Interestingly, the actor learned a primarily defensive approach, likely due to the initial rewards favoring proximity to the ball.

#### Figure 2: **Agent learned defensive playing**

![Performance Metrics](/img/reinforcement/reinforcementl3.png)

These metrics show a clear improvement in the agent's ability to prevent opponent scoring, though offensive capabilities remained limited.

This project has been an eye-opener to the complexities of RL. It's fascinating how RL attempts to mimic biological learning, yet significant differences remain:

1. **Adaptability**: Living beings can adapt their policies over time, while in RL, the environment and agent are often fixed.
2. **Perception Enhancement**: Humans can create tools to enhance their perception of the environment. How might we replicate this in RL?

# Conclusion

Implementing REINFORCE in SuperTuxKart has been a challenging yet rewarding experience. While I've made progress in creating an agent that can play defensively, there's still much to explore in offensive strategies and more complex behaviors. This project has highlighted the immense potential of RL while also underscoring the significant gap between theory and practice. Check out the full repository [here](https://gitlab.com/davidroza/super-tux-kart-rl).

**Python modules used**:

- [PyTorch](https://pytorch.org/)
- [Ray](https://www.ray.io/)
- [PySuperTuxKart](https://pypi.org/project/PySuperTuxKart/)
