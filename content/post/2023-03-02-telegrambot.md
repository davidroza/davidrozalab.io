---
title: Building a Personalized News Feed Telegram Bot
subtitle: Part 1 - Developing the initial MVP bot
date: 2023-03-02
tags: ["c++", "python", "nlp", "ai", "mongodb"]
---

In today's digital age, we're inundated with information, but finding relevant and engaging content can be a challenge. For months, I found myself frustrated with my news feed. Despite the advanced algorithms employed by major platforms, I often struggled to find articles that genuinely interested me. The endless scrolling and sifting through content based on cookie-driven recommendations left me feeling unfulfilled and wasting precious time.

<!--more-->

# The Solution: A Custom Telegram Bot for Personalized News

To address this issue, I embarked on an ambitious project: developing a Telegram bot that would fetch and deliver news tailored to my specific interests and preferences. This bot would eliminate the need for aimless browsing and provide me with a curated selection of articles I actually want to read.

## Project Overview: A Three-Pronged Approach

This project is divided into three main components, each presenting its own unique challenges and learning opportunities:

1. **Telegram Bot Development (C++)**: Creating a robust and efficient bot to interact with users through the Telegram platform.
2. **NLP Classifier (Python)**: Implementing a natural language processing model to categorize and filter news articles based on my preferences.
3. **News Crawler (Python)**: Developing a web crawler to periodically fetch fresh content from various news sources.

The final step involves integrating these modules and deploying the entire system on a Raspberry Pi for continuous operation.

## Security Considerations

Given the personal nature of this project and the potential vulnerabilities of running a 24/7 service, I implemented two key security measures:

1. **User Authentication**: The bot is configured to communicate exclusively with my Telegram account, preventing unauthorized access.
2. **Network Protection**: My home network, where the Raspberry Pi is deployed, is fortified with pfSense, providing an additional layer of security.

# Pilot Project: Learning Through Implementation

This post focuses on my initial attempt—a pilot project designed to help me understand the requirements and potential scalability issues. The complete source code for this version is available in [this GitLab repository](https://gitlab.com/davidroza/telegrambot).

## Bot Development in C++

The majority of my time and effort went into developing the Telegram bot using C++. This choice of language presented several challenges:

1. **Inherent Complexity**: C++ is known for its steep learning curve and potential for intricate, low-level programming.
2. **Networking Challenges**: Implementing network-related functionality in C++ adds another layer of complexity.

Despite these hurdles, I found the process of coding and diving deep into the Telegram API to be incredibly rewarding. The [official Telegram Bot API documentation](https://core.telegram.org/bots/api) proved to be an invaluable resource throughout the development process.

For database interactions, I utilized MongoDB with the C++ driver. The [mongocxx official documentation](https://mongocxx.org/) provided essential guidance for integrating MongoDB into the C++ environment.

## First MVP: Lessons Learned

My initial Minimum Viable Product (MVP) was functional but far from optimal. The primary issues were:

1. **Synchronous Architecture**: The bot operated synchronously, which would limit scalability for potential multi-user support in the future.
2. **Inefficient Command Handling**: The method for processing user commands was not as streamlined as it could be.
3. **Message Sequencing Challenges**: Handling sequential messages, particularly for commands like `/add` to append new articles to the database, became problematic and overly complex.

![Add Command Demonstration](/img/telegrambot/add.gif)

## Insights Gained from the Pilot

Deploying this initial architecture into production, despite its shortcomings, proved to be an invaluable learning experience. It highlighted several areas for improvement:

1. **Logging and Testing**: The need for comprehensive logging and a robust testing framework became apparent.
2. **Asynchronous Design**: Implementing an asynchronous architecture would greatly improve performance and scalability.
3. **Message Ownership**: Developing a more sophisticated system for handling message ownership and context.
4. **Network Security**: Running a Raspberry Pi 24/7 exposed potential vulnerabilities in my home network, prompting a reevaluation of security measures.

# Moving Forward

As the adage goes, "Perfect is the enemy of good." This pilot project, though imperfect, provided crucial insights that will inform the next iteration of the bot.

[Next post in the series](/post/2023-11-15-telegrambot).
